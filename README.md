# xf_kicad_lib

#### 介绍
xf_kicad_lib，是KiCad 的额外封装库。

#### 安装教程
1.  安装方法:先从清华大学开源镜像站下载KICAD6.0完整版，地址：https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-6.0.0-x86_64.exe，
安装到D盘（把安装路径的第一个字母改为D即可）。

2. 安装完成以后，新建D:\Program Files\KiCad\share\kicad目录，在目录下同步额外封装库svn://gitee.com/cuiliwei/xf_kicad_lib；

3. 把D:\Program Files\KiCad\6.0\share\kicad\3dmodels 移动到D:\Program Files\KiCad\share\kicad\modules\ 目录并重命名为packages3d。

4. 在D:\Program Files\KiCad\share\kicad\modules\packages3d目录下新建xf_Library.3dshapes目录，并同步额外3D库：svn://gitee.com/cuiliwei/xf_kicad_3d 。

5. 把配置文件D:\Program Files\KiCad\share\kicad\6.0.zip解压缩到 C:\Users\$你的用户名$\AppData\Roaming\kicad\6.0\,覆盖默认文件。

6. 把额外插件D:\Program Files\KiCad\share\kicad\plugins.zip解压缩到 C:\Users\$你的用户名$\\Documents\KiCad\6.0\scripting\plugins\

   
